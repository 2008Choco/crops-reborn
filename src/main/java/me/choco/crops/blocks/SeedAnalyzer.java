package me.choco.crops.blocks;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.UUID;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import me.choco.crops.CropsReborn;
import me.choco.crops.api.CropType;
import me.choco.crops.api.block.ArmorStandData;
import me.choco.crops.api.block.CustomBlock;
import me.choco.crops.api.block.ArmorStandData.ArmorStandPart;
import me.choco.crops.utils.GUI;
import me.choco.crops.utils.general.ItemBuilder;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Hopper;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Directional;
import org.bukkit.util.Vector;

public class SeedAnalyzer extends CustomBlock implements InventoryHolder, Directional {
	
	/* JSON Syntax:
	 * {
	 *     "location": {
	 *         "world": "worldName",
	 *         "x": 0,
	 *         "y": 0,
	 *         "z": 0,
	 *         "orientation": "NORTH"
	 *     },
	 *     "components": [
	 *         "b35a0270-7027-4b43-9e1d-be30b03ca7a5",
	 *         "eb90422c-1664-49c8-9cb6-bd7464145b9b",
	 *         "34616df8-d912-4b39-a870-9279498b5db2"
	 *     ]
	 * }
	 */
	
	public static final ItemStack ITEM = new ItemBuilder(Material.STEP, (byte) 7)
			.setName(ChatColor.GREEN + "Seed Analyzer")
			.addEnchantment(Enchantment.DURABILITY, 1)
			.addFlags(ItemFlag.HIDE_ENCHANTS)
			.build();
	
	// Inventory slots
	public static final int SLOT_INTERNAL_STORAGE = 10;
	public static final int SLOT_STAGING = 13;
	public static final int SLOT_STATUS_DISPLAY = 16;
	public static final int[] SLOTS_PROGRESS = {28, 29, 30, 31, 32, 33, 34};
	
	// Misc. information / constants
	private static final float PERCENT_PER_GLASS = 12.5f;
	private static final Vector ZERO_VECTOR = new Vector(0, 0, 0);
	private static final NumberFormat PERCENT_FORMAT = new DecimalFormat("#0.00");
	
	// Inventory items
	private static final ItemStack ITEM_PROGRESS_WAITING = new ItemBuilder(Material.STAINED_GLASS_PANE, (byte) 7)
			.setName(ChatColor.GRAY.toString() + ChatColor.BOLD + "Waiting...").build();
	private static final ItemStack ITEM_PROGRESS_COMPLETE = new ItemBuilder(Material.STAINED_GLASS_PANE, (byte) 5)
			.setName(ChatColor.GREEN.toString() + ChatColor.BOLD + "Progress: 0%").build();
	private static final ItemStack ITEM_PROGRESS_INCOMPLETE = new ItemBuilder(Material.STAINED_GLASS_PANE, (byte) 14)
			.setName(ChatColor.RED.toString() + ChatColor.BOLD + "Progress: 0%").build();
	
	private static final ItemStack ITEM_INTERNAL_STORAGE = new ItemBuilder(Material.CHEST)
			.setName(ChatColor.GREEN + "Internal Storage").build();
	private static final ItemStack ITEM_STAGING = new ItemBuilder(Material.STEP)
			.setName(ChatColor.GRAY.toString() + ChatColor.BOLD + "Staging").build();
	private static final ItemStack ITEM_STATUS_DISPLAY = new ItemBuilder(Material.PAPER)
			.setName(ChatColor.YELLOW + "Analyzer Status")
			.setLore(Arrays.asList(
					ChatColor.GRAY + "   Analyzing: " + ChatColor.GREEN + "%analyzing%",
					ChatColor.GRAY + "   Status: " + ChatColor.GREEN + "%status%")
			).build();
	
	// Armour stand data
	private static final ItemStack ITEM_CROP_ANALYZER_FRAME = new ItemStack(Material.THIN_GLASS);
	private static final ItemStack ITEM_CROP_ANALYZER_SUPPORT_STICK = new ItemStack(Material.STICK);
	private static final ItemStack ITEM_CROP_ANALYZER_PLATE = new ItemStack(Material.STEP);
	
	private static final ArmorStandData AS_DATA_BASE = CustomBlock.newArmorStandData()
			.withPose(ArmorStandPart.HEAD, Math.PI, 0, 0)
			.withPose(ArmorStandPart.LEFT_ARM, Math.toRadians(90), Math.toRadians(270), 0)
			.withPose(ArmorStandPart.RIGHT_ARM, Math.toRadians(218), 2 * Math.PI, 2 * Math.PI)
			.withPose(ArmorStandPart.LEFT_LEG, Math.PI, 0, 0)
			.withPose(ArmorStandPart.RIGHT_LEG, Math.PI, 0, 0);
	private static final ArmorStandData AS_DATA_PLATE = CustomBlock.newArmorStandData()
			.withPose(ArmorStandPart.LEFT_ARM, Math.toRadians(90), Math.toRadians(90), 0);
	
	private static final BlockFace[] HOPPER_INPUTS = {BlockFace.UP, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.EAST, BlockFace.WEST};
	
	/* Inventory Slot Layout:
	 * -- -- -- -- -- -- -- -- --
	 * -- 10 -- -- 13 -- -- 16 --
	 * -- -- -- -- -- -- -- -- --
	 * -- 28 29 30 31 32 33 34 --
	 * -- -- -- -- -- -- -- -- --
	 */
	private final GUI inventory;
	private final Inventory internalStorage;
	
	private Item analyzing;
	private int analyzingAmount;
	private float analyzingProgress = 0.0f;
	private boolean isAnalyzing = false;
	
	private final Block block;
	private final Location location;
	private BlockFace facing;
	
	private int lastHopperTransfer = 0;
	
	private ArmorStand[] armourStands = new ArmorStand[3];
	
	public SeedAnalyzer(Location location, BlockFace facing) {
		location = location.getBlock().getLocation();
		if (facing != BlockFace.NORTH && facing != BlockFace.SOUTH
				&& facing != BlockFace.EAST && facing != BlockFace.WEST)
			throw new IllegalArgumentException("Cannot set the direction of the Seed Analyzer to " + facing);
		
		this.location = location;
		this.block = location.getBlock();
		this.facing = facing;
		this.inventory = new GUI(Bukkit.createInventory(this, 45, ChatColor.GREEN + "Seed Analyzer"));
		this.internalStorage = Bukkit.createInventory(this, 18, ChatColor.YELLOW + "Internal Storage");
		
		// Initialize GUI Items & Actions
		this.initializeInventory();
	}
	
	public SeedAnalyzer(Block block, BlockFace facing) {
		this(block.getLocation(), facing);
	}
	
	/**
	 * Get the Seed Analyzer block position
	 * 
	 * @return the analyzer block
	 */
	public Block getBlock() {
		return block;
	}
	
	/**
	 * Set the ItemStack being analyzed by the Seed Analyzer with a specific amount. Setting 
	 * the item to null will remove the item, similar to calling {@link #setAnalyzing(int)} 
	 * with a parameter of 0
	 * 
	 * @param analyzing the item to analyze
	 * @param amount the amount of the item to analyze
	 */
	public void setAnalyzing(ItemStack analyzing, int amount) {
		if (this.analyzing != null) this.analyzing.remove();
		
		if (analyzing != null) {
			analyzing.setAmount(amount);
			Item item = this.location.getWorld().dropItem(location.clone().add(0.5, 0.6, 0.5), analyzing);
			item.setPickupDelay(32767); // Cannot pickup item
			item.setVelocity(ZERO_VECTOR);
			this.analyzing = item;
			
			this.analyzingAmount = amount;
			this.inventory.setItem(SLOT_STAGING, analyzing);
		}
		else {
			this.analyzing.remove();
			this.inventory.setItem(SLOT_STAGING, ITEM_STAGING);
			this.analyzingAmount = 0;
			this.analyzingProgress = 0;
			this.stopAnalyzing();
			
			// Reset progress bar
			for (int i : SLOTS_PROGRESS) {
				this.inventory.setItem(i, ITEM_PROGRESS_WAITING);
			}
		}
	}
	
	/**
	 * Set the ItemStack being analyzed by the Seed Analyzer. Setting the item to null will
	 * remove the item, similar to calling {@link #setAnalyzing(int)} with a parameter of 0
	 * 
	 * @param analyzing the item to analyze
	 */
	public void setAnalyzing(ItemStack analyzing) {
		this.setAnalyzing(analyzing, analyzing != null ? analyzing.getAmount() : 1);
	}
	
	/**
	 * Set the amount remaining of the item being analyzed. The method will fail silently
	 * if no item is currently being analyzed. Setting the amount to 0 will remove the item,
	 * similar to calling {@link #setAnalyzing(ItemStack)} with a null parameter
	 * 
	 * @param amount the amount to set
	 */
	public void setAnalyzing(int amount) {
		if (this.analyzing == null) return;
		
		if (amount <= 0) {
			this.setAnalyzing(null);
		}
		else {
			this.setAnalyzing(this.inventory.getInventory().getItem(SLOT_STAGING), amount);
		}
	}
	
	/**
	 * Get the ItemStack currently being analyzed. May be null
	 * 
	 * @return the item being analyzed. null if none
	 */
	public ItemStack getAnalyzing() {
		if (analyzing == null) return null;
		return analyzing.getItemStack();
	}
	
	/**
	 * Get the amount remaining of the item being analyzed
	 * 
	 * @return the amount of items being analyzed
	 */
	public int getAnalyzingCount() {
		if (analyzing == null) return -1;
		return analyzingAmount;
	}
	
	/**
	 * Set the progress towards a full analysis of the item currently being analyzed. The
	 * value must be between 0.0 (not at all) and 1.0 (completed analyzed)
	 * 
	 * @param analyzingProgress the new progress
	 */
	public void setAnalyzingProgress(float analyzingProgress) {
		this.analyzingProgress = analyzingProgress;
	}
	
	/**
	 * Get the progress towards a full analysis of the item currently being analyzed. The 
	 * returned value will be between 0.0 (not at all) and 1.0 (completely analyzed)
	 * 
	 * @return the progress until fully analyzed.
	 */
	public float getAnalyzingProgress() {
		return analyzingProgress;
	}
	
	/**
	 * Whether this seed analyzer is in progress or if it is idle and not analyzing its
	 * staged seed
	 * 
	 * @return true if analyzing, false otherwise
	 */
	public boolean isAnalyzing() {
		return isAnalyzing;
	}
	
	/**
	 * Start the analyzing process. If no seed is available on the staging platform to
	 * analyze, this method will silently fail
	 * 
	 * @return false if already analyzing, or if no seed to analyze
	 */
	public boolean startAnalyzing() {
		if (isAnalyzing || analyzing == null) return false;
		this.isAnalyzing = true;
		return true;
	}
	
	/**
	 * Stop the analyzing process
	 * 
	 * @return false if already stopped
	 */
	public boolean stopAnalyzing() {
		if (!isAnalyzing) return false;
		this.isAnalyzing = false;
		return true;
	}
	
	/**
	 * Get the inventory holding the Seed Analyzer's internal storage
	 * 
	 * @return the internal storage inventory
	 */
	public Inventory getInternalStorage() {
		return internalStorage;
	}
	
	/**
	 * Update the Seed Analyzer's main inventory's GUI components
	 */
	public void updateInventory() {
		if (inventory.getInventory().getViewers().size() == 0) return; // Don't update if not necessary
		
		ItemStack complete = ITEM_PROGRESS_COMPLETE.clone();
		ItemStack incomplete = ITEM_PROGRESS_INCOMPLETE.clone();
		
		/* Update glass percentages in name */
		int completedGlass = (int) Math.floor((analyzingProgress * 100.0f) / PERCENT_PER_GLASS);
		
		String percentComplete = PERCENT_FORMAT.format((this.analyzingProgress * 100.0f));
		if (completedGlass > 0) {
			complete = new ItemBuilder(complete)
					.setName(ChatColor.GREEN.toString() + ChatColor.BOLD + "Progress: " + percentComplete + "%")
					.build();
		}
		if (completedGlass < 7) {
			incomplete = new ItemBuilder(incomplete)
					.setName(ChatColor.RED.toString() + ChatColor.BOLD + "Progress: " + percentComplete + "%")
					.build();
		}
		
		for (int i = 0; i < SLOTS_PROGRESS.length; i++) {
			this.inventory.setItem(SLOTS_PROGRESS[i], (i < completedGlass) ? complete : incomplete);
		}
	}
	
	public void setLastHopperTransfer(int lastHopperTransfer) {
		this.lastHopperTransfer = lastHopperTransfer;
	}
	
	public int getLastHopperTransfer() {
		return lastHopperTransfer;
	}
	
	public boolean acceptHopperInput() {
		boolean success = false;
		ItemStack itemToTransfer = null;
		
		for (BlockFace face : HOPPER_INPUTS) {
			Block relative = this.block.getRelative(face);
			if (relative.getType() != Material.HOPPER) continue;
			
			Hopper hopper = (Hopper) relative.getState();
			if (hopper.getInventory().getContents().length == 0) continue;
			
			for (ItemStack hopperItem : hopper.getInventory()) {
				if (hopperItem == null) continue;
				
				if ((this.analyzing != null && this.getAnalyzing().isSimilar(hopperItem) && analyzingAmount < 64)
						|| !this.isAnalyzing) {
					success = true;
					itemToTransfer = new ItemStack(hopperItem);
					itemToTransfer.setAmount(1);
					this.decrementItemCount(hopperItem);
					break;
				}
			}
			
			// Actually move the item
			if (success) {
				ItemStack toAnalyze = this.getAnalyzing();
				
				if (toAnalyze == null) toAnalyze = itemToTransfer;
				else toAnalyze.setAmount(analyzingAmount + 1);
				
				this.setAnalyzing(toAnalyze);
				if (!this.isAnalyzing) {
					this.startAnalyzing();
				}
			}
		}
		
		return success;
	}
	
	private final void initializeInventory() {
		this.inventory.setItem(SLOT_INTERNAL_STORAGE, ITEM_INTERNAL_STORAGE, (player, gui, item, holding, clickType) -> {
			Bukkit.getScheduler().runTaskLater(CropsReborn.getPlugin(CropsReborn.class), () -> {
				player.openInventory(this.getInternalStorage());
			}, 1);
			return true;
		});
		
		this.inventory.setItem(SLOT_STAGING, ITEM_STAGING, (player, gui, item, holding, clickType) -> {
			if (ITEM_STAGING.isSimilar(item)) {
				if (holding == null || !CropType.isCropSeed(holding.getType())) return false;
				
				this.setAnalyzing(holding);
				this.startAnalyzing();
				player.setItemOnCursor(null);
			}
			else {
				if (holding != null && CropType.isCropSeed(holding.getType())) {
					this.setAnalyzing(analyzingAmount + holding.getAmount());
					player.setItemOnCursor(null);
				}
				else {
					this.stopAnalyzing();
					player.setItemOnCursor(this.getAnalyzing());
					this.setAnalyzing(null);
				}
			}
			
			return false;
		});
		
		this.inventory.setItem(SLOT_STATUS_DISPLAY, ITEM_STATUS_DISPLAY);
		
		for (int i : SLOTS_PROGRESS) {
			this.inventory.setItem(i, ITEM_PROGRESS_WAITING);
		}
	}
	
	@Override
	public Location getLocation() {
		return location.clone();
	}
	
	@Override
	public void setFacingDirection(BlockFace facing) {
		if (facing != BlockFace.NORTH && facing != BlockFace.SOUTH
				&& facing != BlockFace.EAST && facing != BlockFace.WEST)
			throw new IllegalArgumentException("Cannot set the direction of the Seed Analyzer to " + facing);
			
		this.facing = facing;
		
		// Update block
		this.destroy();
		this.placeInWorld();
	}
	
	@Override
	public BlockFace getFacing() {
		return facing;
	}
	
	@Override
	public Inventory getInventory() {
		return inventory.getInventory();
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void placeInWorld() {
		World world = this.location.getWorld();
		this.block.setType(Material.STEP);
		this.block.setData((byte) 7); // Quartz slab
		
		for (int i = 0; i < armourStands.length; i++) {
			Location spawnLocation = this.block.getLocation();
			if (i == 0) {
				if (facing == BlockFace.NORTH) spawnLocation.add(0.09, -0.97, 0.45);
				if (facing == BlockFace.EAST) spawnLocation.add(0.55, -0.97, 0.09);
				if (facing == BlockFace.SOUTH) spawnLocation.add(0.91, -0.97, 0.55);
				if (facing == BlockFace.WEST) spawnLocation.add(0.45, -0.97, 0.91);
				
				spawnLocation.setYaw(180 + (facing.ordinal() * 90));
			}
			else if (i == 1) {
				if (facing == BlockFace.NORTH) spawnLocation.add(0.54, -1.30, 0.58);
				if (facing == BlockFace.EAST) spawnLocation.add(0.42, -1.30, 0.54);
				if (facing == BlockFace.SOUTH) spawnLocation.add(0.46, -1.30, 0.42);
				if (facing == BlockFace.WEST) spawnLocation.add(0.58, -1.30, 0.46);
				
				spawnLocation.setYaw(facing.ordinal() * 90);
			}
			else if (i == 2) {
				spawnLocation.add(0.5, -1.15, 0.5);
			}
			
			int iteration = i; // Effectively final variable
			ArmorStand armourStand = world.spawn(spawnLocation, ArmorStand.class, a -> {
				a.setArms(true);
				a.setBasePlate(false);
				a.setGravity(false);
				a.setInvulnerable(true);
				a.setVisible(false);
				a.setMarker(true);
				
				// Angles
				a.setLeftArmPose(AS_DATA_BASE.getPose(ArmorStandPart.LEFT_ARM));
				a.setLeftLegPose(AS_DATA_BASE.getPose(ArmorStandPart.LEFT_LEG));
				a.setRightLegPose(AS_DATA_BASE.getPose(ArmorStandPart.RIGHT_LEG));
				
				if (iteration == 0) { // Seed Analyzer Frame
					a.setItemInHand(ITEM_CROP_ANALYZER_FRAME);
					a.setHeadPose(AS_DATA_BASE.getPose(ArmorStandPart.HEAD));
					a.setRightArmPose(AS_DATA_BASE.getPose(ArmorStandPart.RIGHT_ARM));
				}
				else if (iteration == 1) { // Seed Analyzer Support Stick
					a.setItemInHand(ITEM_CROP_ANALYZER_SUPPORT_STICK);
					a.setHeadPose(AS_DATA_BASE.getPose(ArmorStandPart.HEAD));
					a.setRightArmPose(AS_DATA_BASE.getPose(ArmorStandPart.RIGHT_ARM));
				}
				else if (iteration == 2) { // Seed Analyzer Plate
					a.setHelmet(ITEM_CROP_ANALYZER_PLATE);
					a.setHeadPose(AS_DATA_PLATE.getPose(ArmorStandPart.HEAD));
					a.setRightArmPose(AS_DATA_PLATE.getPose(ArmorStandPart.LEFT_ARM));
				}
			});
			
			this.armourStands[i] = armourStand;
		}
	}

	@Override
	public void destroy() {
		for (int i = 0; i < armourStands.length; i++) {
			armourStands[i].remove();
			armourStands[i] = null;
		}
		
		this.block.setType(Material.AIR);
		if (this.analyzing != null) this.analyzing.remove();
	}

	@Override
	public boolean validate(boolean destroyIfInvalid) {
		boolean valid = (this.block.getType() != Material.STEP);
		for (int i = 0; i < armourStands.length; i++)
			if (!armourStands[i].isValid()) valid = false;
		
		if (!valid && destroyIfInvalid) this.destroy();
		return valid;
	}
	
	@Override
	public void onClick(Player player, PlayerInteractEvent event) {
		if (event.getAction() != Action.RIGHT_CLICK_BLOCK || player.isSneaking() 
				|| event.getHand() != EquipmentSlot.HAND) return;
		
		this.inventory.open(player);
		event.setCancelled(true);
	}
	
	private void decrementItemCount(ItemStack item) {
		int amount = item.getAmount();
		item.setAmount(amount > 1 ? amount - 1 : 0);
	}
	
	public static class TypeAdapter implements JsonSerializer<SeedAnalyzer>, JsonDeserializer<SeedAnalyzer> {

		@Override
		public JsonElement serialize(SeedAnalyzer analyzer, Type type, JsonSerializationContext context) {
			JsonObject root = new JsonObject();
			
			// Location serialization
			JsonObject locationData = new JsonObject();
			locationData.addProperty("world", analyzer.location.getWorld().getUID().toString());
			locationData.addProperty("x", analyzer.location.getBlockX());
			locationData.addProperty("y", analyzer.location.getBlockY());
			locationData.addProperty("z", analyzer.location.getBlockZ());
			locationData.addProperty("orientation", analyzer.facing.name());
			
			// Component (armour stand) serialization
			JsonArray componentData = new JsonArray();
			for (ArmorStand armourStand : analyzer.armourStands)
				componentData.add(new JsonPrimitive(armourStand.getUniqueId().toString()));
			
			root.add("location", locationData);
			root.add("components", componentData);
			// TODO: Inventory data
			return root;
		}
		
		@Override
		public SeedAnalyzer deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException {
			if (!element.isJsonObject()) return null;
			JsonObject root = element.getAsJsonObject();
			
			// Location parsing
			JsonObject locationData = root.get("location").getAsJsonObject();
			World world = Bukkit.getWorld(UUID.fromString(locationData.get("world").getAsString()));
			int x = locationData.get("x").getAsInt(), y = locationData.get("y").getAsInt(), z = locationData.get("z").getAsInt();
			
			BlockFace facing = BlockFace.valueOf(locationData.get("orientation").getAsString());
			Location location = new Location(world, x, y, z);
			
			// Component (armour stand) parsing
			JsonArray componentData = root.get("components").getAsJsonArray();
			ArmorStand[] armourStands = new ArmorStand[componentData.size()];
			
			for (int i = 0; i < armourStands.length; i++) {
				UUID uuid = UUID.fromString(componentData.get(i).getAsString());
				armourStands[i] = (ArmorStand) Bukkit.getEntity(uuid);
			}
			
			SeedAnalyzer analyzer = new SeedAnalyzer(location, facing);
			analyzer.armourStands = armourStands;
			return analyzer;
		}
		
	}
	
}