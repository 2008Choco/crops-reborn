package me.choco.crops.blocks.task;

import java.util.Collection;
import java.util.Random;

import org.bukkit.scheduler.BukkitRunnable;

import me.choco.crops.CropsReborn;
import me.choco.crops.api.ModifiedCrop;
import me.choco.crops.api.modtype.RandomTickModifier;
import me.choco.crops.blocks.CropSupports;
import me.choco.crops.utils.CropManager;

public class CropSupportsTickTimer extends BukkitRunnable {
	
	private static final Random RANDOM = new Random();
	private static CropSupportsTickTimer instance;
	
	private CropManager cropManager;
	
	private CropSupportsTickTimer(CropsReborn plugin) {
		this.cropManager = plugin.getCropManager();
	}
	
	@Override
	public void run() {
		// Crops with RandomTickModifier modifiers
		Collection<ModifiedCrop> crops = this.cropManager.getModifiedCrops();
		if (crops.size() > 0) {
			crops.forEach(c -> c.getModifiers()
					.stream()
					.filter(m -> m instanceof RandomTickModifier)
					.forEach(m -> ((RandomTickModifier) m).onRandomTick(c, RANDOM))
			);
		}
		
		// CropSupports recipe breeding
		Collection<CropSupports> allSupports = this.cropManager.getCustomBlocks(CropSupports.class);
		allSupports.forEach(s -> {
			if (RANDOM.nextDouble() * 100 < CropSupports.BREED_CHANCE)
				s.breedSurroundingCrops();
		});
	}
	
	/**
	 * Start the random tick timer
	 * 
	 * @param plugin an instance of CropsReborn to start the timer
	 * @return the created RandomTickTimer instance
	 */
	public static CropSupportsTickTimer startTimer(CropsReborn plugin) {
		if (instance != null) throw new IllegalStateException("There can only be one random tick timer");
		if (plugin == null) throw new IllegalArgumentException("Cannot start a timer with a null plugin");
		
		instance = new CropSupportsTickTimer(plugin);
		instance.runTaskTimer(plugin, 0, 1);
		return instance;
	}
}