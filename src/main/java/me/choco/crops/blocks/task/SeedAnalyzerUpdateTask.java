package me.choco.crops.blocks.task;

import java.util.Set;

import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import me.choco.crops.CropsReborn;
import me.choco.crops.blocks.SeedAnalyzer;
import me.choco.crops.utils.CropManager;
import me.choco.crops.utils.general.ItemBuilder;

public class SeedAnalyzerUpdateTask extends BukkitRunnable {
	
	private static final float PROGRESS_INCREMENT = 0.01f;
	private static SeedAnalyzerUpdateTask instance;
	
	private final CropManager cropManager;
	
	public SeedAnalyzerUpdateTask(CropsReborn plugin) {
		this.cropManager = plugin.getCropManager();
	}
	
	@Override
	public void run() {
		if (this.cropManager.getCustomBlocks().size() == 0) return;
		
		Set<SeedAnalyzer> analyzers = this.cropManager.getCustomBlocks(SeedAnalyzer.class);
		
		for (SeedAnalyzer analyzer : analyzers) {
			// Hopper updates
			this.attemptHopperInputFor(analyzer);
			
			if (!analyzer.isAnalyzing()) continue;
			
			// Inventory space checks
			if (!this.hasSlotAvailableFor(analyzer.getInternalStorage(), analyzer.getAnalyzing().getType())) {
				analyzer.stopAnalyzing();
				analyzer.updateInventory();
				continue;
			}
			
			// Progress updates
			this.attemptProgressUpdateFor(analyzer);
			analyzer.updateInventory();
		}
	}
	
	private void attemptHopperInputFor(SeedAnalyzer analyzer) {
		int lastHopperTransfer = analyzer.getLastHopperTransfer();
		if (lastHopperTransfer++ >= 8) {
			analyzer.acceptHopperInput();
			lastHopperTransfer = 0;
		}
		analyzer.setLastHopperTransfer(lastHopperTransfer);
	}
	
	private void attemptProgressUpdateFor(SeedAnalyzer analyzer) {
		float progress = analyzer.getAnalyzingProgress() + PROGRESS_INCREMENT;
		if (progress >= 1.0) {
			analyzer.setAnalyzingProgress(0);
			analyzer.setAnalyzing(analyzer.getAnalyzingCount() - 1);
			
			// Add item to internal storage
			ItemStack seed = new ItemBuilder(analyzer.getAnalyzing()).setAmount(1).build();
			analyzer.getInternalStorage().addItem(CropsReborn.researchCropItem(seed));
		}
		else {
			analyzer.setAnalyzingProgress(progress);
		}
	}
	
	private boolean hasSlotAvailableFor(Inventory inventory, Material material) {
		for (ItemStack item : inventory.getContents()) {
			if (item == null) return true;
			if (item.getType() == material && item.getAmount() <= 63) return true;
		}
		return false;
	}
	
	/**
	 * Start the analyzer update task
	 * 
	 * @param plugin an instance of CropsReborn to start the timer
	 * @return the created AnalyzeUpdateTask instance
	 */
	public static SeedAnalyzerUpdateTask startTimer(CropsReborn plugin) {
		if (instance != null) throw new IllegalStateException("There can only be one random tick timer");
		if (plugin == null) throw new IllegalArgumentException("Cannot start a timer with a null plugin");
		
		instance = new SeedAnalyzerUpdateTask(plugin);
		instance.runTaskTimer(plugin, 0, 1);
		return instance;
	}
	
}