package me.choco.crops.blocks;

import java.lang.reflect.Type;
import java.util.UUID;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import me.choco.crops.CropsReborn;
import me.choco.crops.api.ModifiedCrop;
import me.choco.crops.api.block.ArmorStandData;
import me.choco.crops.api.block.CustomBlock;
import me.choco.crops.api.block.ArmorStandData.ArmorStandPart;
import me.choco.crops.utils.general.ItemBuilder;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ArmorStand;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Represents crop supports, the base of all crop breeding. This object manages the breeding of
 * crops, as well as the management of random events on the crop it's supporting
 */
public class CropSupports extends CustomBlock {
	
	/* JSON Syntax:
	 * {
	 *     "crop": "548c44d0-de0b-4d2a-bed4-573a14b8df90",
	 *     "location": {
	 *         "world": "worldName",
	 *         "x": 0,
	 *         "y": 0,
	 *         "z": 0,
	 *     },
	 *     "components": [
	 *         "b35a0270-7027-4b43-9e1d-be30b03ca7a5",
	 *         "eb90422c-1664-49c8-9cb6-bd7464145b9b",
	 *         "34616df8-d912-4b39-a870-9279498b5db2",
	 *         "2d83a1b8-c5a0-43db-a015-805b90cccaac"
	 *     ]
	 * }
	 */
	
	public static final double BREED_CHANCE = 2.5;
	public static final ItemStack ITEM = new ItemBuilder(Material.STICK)
			.setName(ChatColor.GOLD + "Crop Supports")
			.addEnchantment(Enchantment.DURABILITY, 1)
			.addFlags(ItemFlag.HIDE_ENCHANTS).build();
	
	private static final ItemStack CROP_SUPPORT_STICK = new ItemStack(Material.STICK);
	private static final ArmorStandData AS_DATA = CustomBlock.newArmorStandData()
			.withPose(ArmorStandPart.HEAD, Math.PI, 0, 0)
			.withPose(ArmorStandPart.LEFT_ARM, Math.toRadians(90), Math.toRadians(270), 0)
			.withPose(ArmorStandPart.RIGHT_ARM, Math.toRadians(260), Math.toRadians(270), 0)
			.withPose(ArmorStandPart.LEFT_LEG, Math.PI, 0, 0)
			.withPose(ArmorStandPart.RIGHT_LEG, Math.PI, 0, 0);
	
	private final Block soil;
	private final Location location;
	private ModifiedCrop crop;
	
	private ArmorStand[] armourStands = new ArmorStand[4];
	
	public CropSupports(Block soil, ModifiedCrop crop) {
		this.soil = soil;
		this.location = soil.getRelative(BlockFace.UP).getLocation();
		this.crop = crop;
	}
	
	public CropSupports(Location location, ModifiedCrop crop) {
		this.soil = location.getBlock().getRelative(BlockFace.DOWN);
		this.location = location.getBlock().getLocation();
		this.crop = crop;
	}
	
	public CropSupports(Block soil) {
		this(soil, null);
	}
	
	public CropSupports(Location location) {
		this(location, null);
	}
	
	/**
	 * Get the block that these supports are sitting on (The tilled soil)
	 * 
	 * @return the tilled soil
	 */
	public Block getSoil() {
		return soil;
	}
	
	
	/**
	 * Set the supported modified crop
	 * 
	 * @param crop the crop to set
	 */
	public void setCrop(ModifiedCrop crop) {
		this.crop = crop;
	}
	
	/**
	 * Get the supported modified crop
	 * 
	 * @return the modified crop. null if no crop present
	 */
	public ModifiedCrop getCrop() {
		return crop;
	}
	
	/**
	 * Whether a crop is being supported or not
	 * 
	 * @return true if a crop is present, false otherwise
	 */
	public boolean hasCrop() {
		return this.crop != null;
	}
	
	@Override
	public Location getLocation() {
		return location.clone();
	}

	@Override
	public void placeInWorld() {
		World world = this.location.getWorld();
		
		for (int i = 0; i < armourStands.length; i++) {
			/*    Aerial View:
			     FAR-L    FAR-R
			    CLOSE-L  CLOSE-R   */
			
			double xOffset = (i == 0 || i == 2 ? -0.075 /* Close Left & Far Left */ : 0.725 /* Close Right & Far Right */);
			double zOffset = (i == 0 || i == 1 ? 0.05 /* Close Left & Far Left */ : 0.85 /* Close Right & Far Right */);
			Location spawnLocation = new Location(world, location.getBlockX() + xOffset, location.getBlockY() - 1.5, location.getBlockZ() + zOffset);
			
			ArmorStand cropSupport = world.spawn(spawnLocation, ArmorStand.class, a -> {
				a.setArms(true);
				a.setBasePlate(false);
				a.setGravity(false);
				a.setInvulnerable(true);
				a.setVisible(false);
				a.setMarker(true);
				a.getEquipment().setItemInMainHand(CROP_SUPPORT_STICK);
				
				// Angles
				a.setHeadPose(AS_DATA.getPose(ArmorStandPart.HEAD));
				a.setLeftArmPose(AS_DATA.getPose(ArmorStandPart.LEFT_ARM));
				a.setRightArmPose(AS_DATA.getPose(ArmorStandPart.RIGHT_ARM));
				a.setLeftLegPose(AS_DATA.getPose(ArmorStandPart.LEFT_LEG));
				a.setRightLegPose(AS_DATA.getPose(ArmorStandPart.RIGHT_LEG));
			});
			
			this.armourStands[i] = cropSupport;
		}
	}
	
	@Override
	public void destroy() {
		for (int i = 0; i < armourStands.length; i++) {
			armourStands[i].remove();
			armourStands[i] = null;
		}
	}
	
	@Override
	public boolean validate(boolean destroyIfInvalid) {
		boolean valid = true;
		for (int i = 0; i < armourStands.length; i++)
			if (!armourStands[i].isValid()) valid = false;
		
		if (!valid && destroyIfInvalid) this.destroy();
		return valid;
	}
	
	/**
	 * Breed the surrounding crops, if any breeding recipe is possible
	 * 
	 * @return true if breeding successful, false otherwise
	 */
	public boolean breedSurroundingCrops() {
		if (this.crop != null) return false;
		
		// TODO
		return true;
	}
	
	
	public static class TypeAdapter implements JsonSerializer<CropSupports>, JsonDeserializer<CropSupports> {
		
		@Override
		public JsonElement serialize(CropSupports analyzer, Type type, JsonSerializationContext context) {
			JsonObject root = new JsonObject();
			
			// Root-level serialization
			if (analyzer.hasCrop())
				root.addProperty("crop", analyzer.getCrop().getUUID().toString());
			
			// Location serialization
			JsonObject locationData = new JsonObject();
			locationData.addProperty("world", analyzer.location.getWorld().getUID().toString());
			locationData.addProperty("x", analyzer.location.getBlockX());
			locationData.addProperty("y", analyzer.location.getBlockY());
			locationData.addProperty("z", analyzer.location.getBlockZ());
			
			// Component (armour stand) serialization
			JsonArray componentData = new JsonArray();
			for (ArmorStand armourStand : analyzer.armourStands)
				componentData.add(new JsonPrimitive(armourStand.getUniqueId().toString()));
			
			root.add("location", locationData);
			root.add("components", componentData);
			return root;
		}
		
		@Override
		public CropSupports deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException {
			if (!element.isJsonObject()) return null;
			JsonObject root = element.getAsJsonObject();
			
			// Root-level parsing
			ModifiedCrop crop = null;
			if (root.has("crop")) {
				UUID cropUUID = UUID.fromString(root.get("crop").getAsString());
				crop = JavaPlugin.getPlugin(CropsReborn.class).getCropManager().getCropData(cropUUID);
			}
			
			// Location parsing
			JsonObject locationData = root.get("location").getAsJsonObject();
			World world = Bukkit.getWorld(UUID.fromString(locationData.get("world").getAsString()));
			int x = locationData.get("x").getAsInt(), y = locationData.get("y").getAsInt(), z = locationData.get("z").getAsInt();
			
			Location location = new Location(world, x, y, z);
			
			// Component (armour stand) parsing
			JsonArray componentData = root.get("components").getAsJsonArray();
			ArmorStand[] armourStands = new ArmorStand[componentData.size()];
			
			for (int i = 0; i < armourStands.length; i++) {
				UUID uuid = UUID.fromString(componentData.get(i).getAsString());
				armourStands[i] = (ArmorStand) Bukkit.getEntity(uuid);
			}
			
			CropSupports supports = new CropSupports(location);
			supports.armourStands = armourStands;
			if (crop != null) supports.crop = crop;
			
			return supports;
		}
		
	}
	
}