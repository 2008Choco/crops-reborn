package me.choco.crops.api;

import org.bukkit.Material;

/**
 * A list of all crop types available in Minecraft that can be
 * modified by CropsReborn
 */
public enum CropType {
	
	WHEAT("Wheat", Material.CROPS, Material.SEEDS),
	
	CARROT("Carrot", Material.CARROT, Material.CARROT_ITEM),
	
	POTATO("Potato", Material.POTATO, Material.POTATO_ITEM),
	
	BEETROOT("Beetroot", Material.BEETROOT_BLOCK, Material.BEETROOT_SEEDS),
	
	WATER_MELON_STEM("Watermelon", Material.MELON_STEM, Material.MELON_SEEDS),
	
	PUMPKIN_STEM("Pumpkin", Material.PUMPKIN_STEM, Material.PUMPKIN_SEEDS),
	
	NETHER_WARTS("Nether Wart", Material.NETHER_WART_BLOCK, Material.NETHER_WARTS);

	private final String name;
	private final Material material, seedMaterial;
	
	private CropType (String name, Material material, Material seedMaterial) {
		this.name = name;
		this.material = material;
		this.seedMaterial = seedMaterial;
	}
	
	// Not used, but perhaps in the future it will be?
	private CropType(String name, Material material) {
		this(name, material, material);
	}
	
	/**
	 * Get the friendly name of the crop
	 * 
	 * @return the name of the crop
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Get the material that represents this crop type
	 * 
	 * @return the represented material
	 */
	public Material getMaterial() {
		return material;
	}
	
	/**
	 * Get the material that represents the seed item for this crop type
	 * 
	 * @return the represented seed material
	 */
	public Material getSeedMaterial() {
		return seedMaterial;
	}
	
	/**
	 * Check whether a specific material is an enumerated crop type or not
	 * 
	 * @param material the material to check
	 * @return true if it is a crop, false otherwise
	 */
	public static boolean isCrop(Material material) {
		for (CropType type : values())
			if (type.getMaterial() == material) return true;
		return false;
	}
	
	/**
	 * Check whether a specific material is an enumerated crop type seed or not
	 * 
	 * @param material the material to check
	 * @return true if it is a seed, false otherwise
	 */
	public static boolean isCropSeed(Material material) {
		for (CropType type : values())
			if (type.getSeedMaterial() == material) return true;
		return false;
	}
	
	/**
	 * Get the crop type based on the specified material
	 * 
	 * @param material the crop material
	 * @return the respective CropType. null if none found
	 */
	public static CropType getCropType(Material material) {
		for (CropType type : values())
			if (type.getMaterial() == material) return type;
		return null;
	}
}