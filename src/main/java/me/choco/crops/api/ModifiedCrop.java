package me.choco.crops.api;

import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import com.google.common.collect.Sets;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

/**
 * Represents a crop with various modifiers to affect its growth,
 * yields, and various other environmental factors
 */
public class ModifiedCrop {
	
	/* JSON Syntax:
	 * {
	 *     "uuid": "7f147036-0011-4ca0-908a-57b1327d2573",
	 *     "location": {
	 *         "world": "worldName",
	 *         "x": 0,
	 *         "y": 0,
	 *         "z": 0,
	 *     },
	 *     modifiers: [
	 *         {
	 *             "name": "me.choco.crops.modifiers.SpeedModifier",
	 *             "value": "1.0"
	 *         },
	 *         ... etc. ...
	 *     ]
	 * }
	 */
	
	private UUID uuid;

	private final Block crop;
	private Set<CropModifier<?>> modifiers;
	
	public ModifiedCrop(Block crop, Set<CropModifier<?>> modifiers) {
		if (crop == null)
			throw new IllegalArgumentException("The crop cannot be null");
		
		this.uuid = UUID.randomUUID();
		this.crop = crop;
		this.modifiers = modifiers != null ? modifiers : new HashSet<>();
	}
	
	public ModifiedCrop(Block crop, CropModifier<?>... modifiers) {
		this(crop, Sets.newHashSet(modifiers));
	}
	
	public ModifiedCrop(Block crop){
		this(crop, new HashSet<>());
	}
	
	/**
	 * Get the original Bukkit block that represents this modified crop
	 * 
	 * @return the Bukkit block object
	 */
	public Block getCrop() {
		return crop;
	}
	
	/**
	 * Get the unique id associated with this ModifiedCrop
	 * 
	 * @return the crop's unique id
	 */
	public UUID getUUID() {
		return uuid;
	}
	
	/**
	 * Add a modifier value for this crop
	 * 
	 * @param modifier the modifier to set
	 */
	public void addModifier(CropModifier<?> modifier){
		this.modifiers.add(modifier);
	}
	
	/**
	 * Get a modifier on this crop
	 * 
	 * @param modifier the modifier to get
	 * @return the modifier instance, or null if none exists
	 */
	@SuppressWarnings("unchecked")
	public <T extends CropModifier<?>> T getModifier(Class<T> modifier){
		for (CropModifier<?> mod : this.modifiers)
			if (modifier.isInstance(mod)) return (T) mod;
		return null;
	}
	
	/**
	 * Check whether the crop has the specified modifier
	 * 
	 * @param modifier the modifier to check
	 * @return true if the crop has the specified modifier
	 */
	public boolean hasModifier(Class<? extends CropModifier<?>> modifier){
		for (CropModifier<?> cropModifier : modifiers)
			if (cropModifier.getClass().equals(modifier)) return true;
		return false;
	}
	
	/**
	 * Get a list of all modifiers present on this crop
	 * 
	 * @return all modifiers
	 */
	public Set<CropModifier<?>> getModifiers() {
		return modifiers;
	}
	
	public static class TypeAdapter implements JsonSerializer<ModifiedCrop>, JsonDeserializer<ModifiedCrop> {

		@Override
		public JsonElement serialize(ModifiedCrop crop, Type type, JsonSerializationContext context) {
			JsonObject root = new JsonObject();
			
			// Root-level serialization
			root.addProperty("uuid", crop.uuid.toString());
			
			// Location serialization
			Location location = crop.getCrop().getLocation();
			JsonObject locationData = new JsonObject();
			locationData.addProperty("world", location.getWorld().getUID().toString());
			locationData.addProperty("x", location.getBlockX());
			locationData.addProperty("y", location.getBlockY());
			locationData.addProperty("z", location.getBlockZ());
			
			// Crop modifier serialization
			JsonArray modifierData = new JsonArray();
			for (CropModifier<?> modifier : crop.getModifiers()) {
				JsonObject modifierDataObject = new JsonObject();
				modifierDataObject.addProperty("name", modifier.getClass().getName());
				modifierDataObject.addProperty("value", modifier.getModifierValue().toString());
				
				modifierData.add(modifierDataObject);
			}
			
			root.add("location", locationData);
			root.add("modifiers", modifierData);
			return null;
		}
		
		@Override
		public ModifiedCrop deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException {
			if (!(element instanceof JsonObject)) return null;
			JsonObject root = (JsonObject) element;
			
			// Root-level parsing
			UUID uuid = UUID.fromString(root.get("uuid").getAsString());
			
			// Location parsing
			JsonObject locationData = root.get("location").getAsJsonObject();
			World world = Bukkit.getWorld(UUID.fromString(locationData.get("world").getAsString()));
			int x = locationData.get("x").getAsInt(), y = locationData.get("y").getAsInt(), z = locationData.get("z").getAsInt();
			
			Block block = new Location(world, x, y, z).getBlock();
			
			// Modifier parsing
			JsonArray modifierData = root.get("modifiers").getAsJsonArray();
			Set<CropModifier<?>> modifiers = new HashSet<>(modifierData.size());
			for (int i = 0; i < modifiers.size(); i++) {
				JsonObject modifierDataObject = modifierData.get(i).getAsJsonObject();
				
				// TODO: This should be done using a static #deserialize() method in each CropModifier class
				try {
					Class<?> modifierClass = Class.forName(modifierDataObject.get("name").getAsString());
					Object value = modifierDataObject.get(modifierDataObject.get("value").getAsString()).getAsString();
					
					CropModifier<?> modifier = (CropModifier<?>) modifierClass.getConstructors()[0].newInstance(value);
					modifiers.add(modifier);
				} catch (ReflectiveOperationException | IllegalArgumentException | SecurityException e) { continue; }
			}
			
			ModifiedCrop crop = new ModifiedCrop(block);
			crop.uuid = uuid;
			crop.modifiers = modifiers;
			
			return crop;
		}
		
	}
	
}