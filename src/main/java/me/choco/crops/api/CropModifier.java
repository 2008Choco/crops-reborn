package me.choco.crops.api;

import org.bukkit.ChatColor;

import me.choco.crops.api.breeding.CropBreedingRecipe;

/**
 * Modifiers that can be applied to {@link ModifiedCrop} objects
 * 
 * @param <T> the type of data stored within this crop modifier
 */
public abstract class CropModifier<T> {
	
	protected T value;
	
	public CropModifier(T value) {
		this.value = value;
	}
	
	/**
	 * Get the friendly name of the modifier (Displayed in lore)
	 * 
	 * @return the name of the modifier
	 */
	public abstract String getName();
	
	/**
	 * Get the description of the modifier (Displayed in agricultural journal)
	 * 
	 * @return the description of the modifier
	 */
	public abstract String getDescription();
	
	/**
	 * Get the percent chance that this modifier has to be discovered when 
	 * researched in a seed analyzer. 
	 * <p>
	 * A value of 1.0 indicates that it will certainly be applied to the 
	 * analyzed seed, whereas a value of 0.0 indicates that it will never 
	 * be discovered upon researching a seed
	 * 
	 * @return the percent chance of research discovery
	 */
	public abstract float getResearchPercentage();
	
	/**
	 * Get the percent chance that this modifier has to be discovered when
	 * bred with another crop
	 * <p>
	 * A value of 1.0 indicates that it will certainly be discovered to the
	 * resulting analyzed crop, whereas a value of 0.0 indicates that it will
	 * never be discovered upon breeding a crop
	 * 
	 * @param recipe the recipe that was used to breed the crops
	 * @param crops the crops bred in this recipe
	 * 
	 * @return the percent chance of research discovery
	 */
	public abstract float getBreedingPercentage(CropBreedingRecipe recipe, ModifiedCrop... crops);
	
	/**
	 * Check whether the modifier is discoverable upon research
	 * 
	 * @return true if researchable, false otherwise
	 */
	public boolean isResearchable() {
		return this.getResearchPercentage() > 0.0f;
	}
	
	/**
	 * Get the base colour of the modifier (Colour of the name in the lore)
	 * <br>Default: GRAY
	 * 
	 * @return the base colour
	 * @see #getName()
	 */
	public ChatColor getBaseColour() {
		return ChatColor.GRAY;
	}
	
	/**
	 * Get the accent colour of the modifier (Colour of the value in the lore)
	 * <br> Default: GREEN
	 * 
	 * @return the accent colour
	 * @see #getModifierValue()
	 */
	public ChatColor getAccentColour() {
		return ChatColor.GREEN;
	}
	
	/**
	 * Set the value of the modifier
	 * 
	 * @param value the value to set
	 */
	public final void setModifierValue(T value) {
		this.value = value;
	}
	
	/**
	 * Get the value of the modifier
	 * 
	 * @return the modifier value
	 */
	public final T getModifierValue() {
		return value;
	}
	
	/**
	 * Get an array of all crop types this modifier can be applied on
	 * 
	 * @return all applicable crops
	 */
	public CropType[] getApplicableCrops() {
		return CropType.values();
	}
	
	/**
	 * Serialize the crop to a readable format
	 * 
	 * @return the serialized crop in String format
	 */
	public String serialize(){
		return this.getName() + ":" + this.getModifierValue();
	}
	
}