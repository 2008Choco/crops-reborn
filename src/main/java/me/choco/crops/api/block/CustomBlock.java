package me.choco.crops.api.block;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Represents a custom block in the CropsReborn plugin composed of armour
 * stands, custom interactions, etc.
 */
public abstract class CustomBlock {
	
	/**
	 * Get the location of the custom block
	 * 
	 * @return the block's location
	 */
	public abstract Location getLocation();
	
	/**
	 * Place the custom block in the world
	 */
	public abstract void placeInWorld();
	
	/**
	 * Destroy the block if it was placed. This does not unregister the block
	 * from its respective registry, but simply removes it from the world
	 */
	public abstract void destroy();
	
	/**
	 * Check whether the custom block is valid or not. The validity of the custom block is based
	 * upon various factors including the validity of the armour stands that compose the block
	 * <br><b>NOTE: </b>This does not unregister the custom block from its respective registry!
	 * 
	 * @param destroyIfInvalid whether the block will be destroyed if invalid or not
	 * @return true if the block is valid
	 */
	public abstract boolean validate(boolean destroyIfInvalid);
	
	/**
	 * Handle any action when clicked on. May not have any method body if not of significance
	 * 
	 * @param player the player that clicked the block
	 * @param event the event handling the click
	 */
	public void onClick(Player player, PlayerInteractEvent event) { }
	
	/**
	 * Construct and return a new instance of {@link ArmorStandData}
	 * 
	 * @return a new ArmorStandData instance
	 */
	protected static ArmorStandData newArmorStandData() {
		return new ArmorStandData();
	}
	
}