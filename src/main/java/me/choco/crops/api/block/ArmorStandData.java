package me.choco.crops.api.block;

import java.util.EnumMap;
import java.util.Map;
import java.util.function.BiConsumer;

import com.google.common.base.Preconditions;

import org.bukkit.entity.ArmorStand;
import org.bukkit.util.EulerAngle;

/**
 * A utility class to act as a more readable way to handle armour stand positions
 * for {@link CustomBlock} implementations. Instances of this class may be retrieved
 * through the use of {@link CustomBlock#newArmorStandData()}
 * 
 * @author Parker Hawke - 2008Choco
 */
public class ArmorStandData {
	
	private final Map<ArmorStandPart, EulerAngle> poseData = new EnumMap<>(ArmorStandPart.class);
	
	protected ArmorStandData() { }
	
	/**
	 * Set a pose for this armour stand data
	 * 
	 * @param part the part to set
	 * @param pose the Euler angle at which to position the part
	 * 
	 * @return this instance. Allows for chained method calls
	 */
	public ArmorStandData withPose(ArmorStandPart part, EulerAngle pose) {
		this.poseData.put(part, pose);
		return this;
	}
	
	/**
	 * Set a pose for this armour stand data
	 * 
	 * @param part the part to set
	 * @param x the angle for the x axis in radians
	 * @param y the angle for the y axis in radians
	 * @param z the angle for the z axis in radians
	 * 
	 * @return this instance. Allows for chained method calls
	 */
	public ArmorStandData withPose(ArmorStandPart part, double x, double y, double z) {
		this.poseData.put(part, new EulerAngle(x, y, z));
		return this;
	}
	
	/**
	 * Get the pose for the specified armour stand part
	 * 
	 * @param part the part to retrieve the angle for
	 * @return the set pose. {@link EulerAngle#ZERO} if none set
	 */
	public EulerAngle getPose(ArmorStandPart part) {
		return poseData.getOrDefault(part, EulerAngle.ZERO);
	}
	
	/**
	 * Apply any set positions to the provided armour stand. Unlike {@link #getPose(ArmorStandPart)},
	 * if a part has not been set in this data instance, the armour stand part will remain untouched
	 * rather than being set to {@link EulerAngle#ZERO}
	 * 
	 * @param armorStand the armour stand on which this data should be applied
	 */
	public void applyTo(ArmorStand armorStand) {
		Preconditions.checkNotNull(armorStand, "Cannot apply data to null armour stand");
		
		for (ArmorStandPart part : ArmorStandPart.values()) {
			if (!poseData.containsKey(part)) continue; // Intentionally not using #getOrDefault()
			part.applicationFunction.accept(armorStand, poseData.get(part));
		}
	}
	
	/**
	 * Clear any pose data set upon this armour stand data
	 */
	public void clearPoseData() {
		this.poseData.clear();
	}
	
	
	/**
	 * Represents one of the various manipulatable parts on an armour stand
	 */
	public enum ArmorStandPart {
		
		HEAD(ArmorStand::setHeadPose),
		BODY(ArmorStand::setBodyPose),
		LEFT_ARM(ArmorStand::setLeftArmPose),
		RIGHT_ARM(ArmorStand::setRightArmPose),
		LEFT_LEG(ArmorStand::setLeftLegPose),
		RIGHT_LEG(ArmorStand::setRightLegPose);
		
		
		private final BiConsumer<ArmorStand, EulerAngle> applicationFunction;
		
		private ArmorStandPart(BiConsumer<ArmorStand, EulerAngle> applicationFunction) {
			this.applicationFunction = applicationFunction;
		}
		
	}
	
}