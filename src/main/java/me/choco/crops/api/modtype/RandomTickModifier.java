package me.choco.crops.api.modtype;

import java.util.Random;

import me.choco.crops.api.ModifiedCrop;

/**
 * A modifier with an update that can occur at a random time
 */
public interface RandomTickModifier {
	
	/**
	 * Called every time this modifier has been ticked. Update the 
	 * crop respectively
	 * 
	 * @param crop the crop to update
	 * @param random a random instance to allow for random modifications
	 */
	public void onRandomTick(ModifiedCrop crop, Random random);
	
}