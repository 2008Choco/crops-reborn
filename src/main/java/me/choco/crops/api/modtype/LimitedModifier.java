package me.choco.crops.api.modtype;

/**
 * A modifier with a maximum numerical value that cannot be exceeded
 * 
 * @param <T> the type of number for this modifier
 */
public interface LimitedModifier<T extends Number> {
	
	/**
	 * Get the maximum value of this modifier
	 * 
	 * @return the maximum value
	 */
	public T getMaxValue();
	
}