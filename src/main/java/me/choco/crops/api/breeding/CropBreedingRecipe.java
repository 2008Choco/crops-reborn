package me.choco.crops.api.breeding;

import org.apache.commons.lang3.ArrayUtils;

import me.choco.crops.api.CropType;
import me.choco.crops.blocks.CropSupports;

/**
 * A breeding recipe managed by {@linkplain CropSupports}. These recipes
 * can result in more powerful, or even different, crop types
 */
public class CropBreedingRecipe {
	
	private CropType[] requiredCrops = new CropType[4];
	private final CropType result;
	private final boolean mergeModifiers;
	
	public CropBreedingRecipe(CropType result, boolean mergeModifiers) {
		this.result = result;
		this.mergeModifiers = mergeModifiers;
	}
	
	public CropBreedingRecipe(CropType result) {
		this(result, true);
	}
	
	/**
	 * Add a crop type to the recipe
	 * 
	 * @param type the type of crop to add
	 * @return itself. Chainable methods
	 */
	public CropBreedingRecipe addCrop(CropType type) {
		if (this.requiredCrops[3] != null)
			throw new IllegalStateException("Cannot pass more than 4 crop types to a crop recipe");
		
		ArrayUtils.add(requiredCrops, type);
		return this;
	}
	
	/**
	 * Add more than one crop type to the recipe
	 * 
	 * @param type the type to add
	 * @param count the amount of that type to add
	 * 
	 * @return itself. Chainable methods
	 */
	public CropBreedingRecipe addCrop(CropType type, int count) {
		int emptyValues = 0;
		for (CropType crop : requiredCrops)
			if (crop == null) emptyValues++;
		
		if (count > emptyValues)
			throw new IllegalArgumentException("Too many crop types!. (" + count + " > " + emptyValues + ")");
		
		for (int i = 0; i < count; i++) this.addCrop(type);
		return this;
	}
	
	/**
	 * Get the type of crop that will result from this recipe
	 * 
	 * @return the resulting crop type
	 */
	public CropType getResult() {
		return result;
	}
	
	/**
	 * Whether the crop modifiers of the surrounding crops should merge
	 * into the final crop product. If false, random merged modifiers will
	 * start to their default values
	 * 
	 * @return true if modifiers will merge, false otherwise
	 */
	public boolean shouldMergeModifiers() {
		return this.mergeModifiers;
	}
	
	/**
	 * Get all required crops for this breeding recipe to function
	 * 
	 * @return an array of required crops
	 */
	public CropType[] getRequiredCrops() {
		return ArrayUtils.clone(requiredCrops);
	}
	
	/**
	 * Get the amount of crops required for this recipe to function
	 * 
	 * @return the amount of required crops
	 */
	public int getRequiredCropCount() {
		int count = 0;
		for (CropType type : this.requiredCrops)
			if (type == null) count++;
		return count;
	}
}