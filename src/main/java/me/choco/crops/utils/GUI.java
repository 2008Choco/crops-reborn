package me.choco.crops.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Consumer;

import com.google.common.base.Preconditions;

/**
 * A GUI that allows customizability for graphical user interfaces in Minecraft
 * using {@link ItemStack} objects and custom {@link GUIAction}s when interacting
 * 
 * @author Parker Hawke - 2008Choco
 */
public class GUI {

	private static boolean initialized = false;
	private static final Map<UUID, GUI> PLAYER_GUIS = new HashMap<>();

	private Map<Integer, GUIAction> actions = new HashMap<>();

	private GUICloseHook onClose;

	private Inventory inventory;
	private final int size;
	private final String title;

	/**
	 * Construct a new GUI instance with a specific inventory size
	 * 
	 * @param title the title of the GUI
	 * @param size the size of the inventory (multiple of 9, or a size of 5)
	 */
	public GUI(String title, int size) {
		InventoryType type = (size == 5 ? InventoryType.HOPPER : InventoryType.CHEST);
		if (type != InventoryType.HOPPER && size % 9 != 0)
			throw new IllegalArgumentException("Inventory sizes must be a multiple of 9, or a size of 5");

		this.size = size;
		this.title = title;
		this.inventory = (type == InventoryType.ANVIL)
				? Bukkit.createInventory(null, type, title)
				: Bukkit.createInventory(null, size, title);
	}
	
	/**
	 * Construct a new GUI instance with a specific inventory type
	 * 
	 * @param title the title of the GUI
	 * @param type the type of inventory to create
	 */
	public GUI(String title, InventoryType type) {
		Preconditions.checkNotNull(type, "Cannot create inventory of type null");
		
		this.size = type.getDefaultSize();
		this.title = title;
		this.inventory = Bukkit.createInventory(null, type, title);
	}
	
	/**
	 * Construct a new GUI instance based on an existing inventory
	 * 
	 * @param inventory the existing inventory to wrap
	 */
	public GUI(Inventory inventory) {
		Preconditions.checkNotNull(inventory, "Cannot create a GUI with a null inventory reference");
		
		this.size = inventory.getSize();
		this.title = inventory.getTitle();
		this.inventory = inventory;
	}

	/**
	 * Get the inventory wrapped by this GUI
	 * 
	 * @return the wrapped inventory
	 */
	public Inventory getInventory() {
		return inventory;
	}

	/**
	 * Get the size of the GUI
	 * 
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * Get the title of the GUI
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Get the GUI action associate with the specified slot
	 * 
	 * @param slot the slot to reference
	 * @return the associated action. null if none
	 */
	public GUIAction getActionForSlot(int slot) {
		return actions.get(slot);
	}

	/**
	 * Set / Replace an item in the GUI with an action to be set alongside the slot 
	 * number. Note that the action is associated with the slot the item is placed 
	 * in, and not with the ItemStack itself
	 * 
	 * @param slot the slot to place the item
	 * @param item the item to add
	 * @param action the action to add. null if no action should occur
	 * 
	 * @return true if successfully set. false otherwise
	 * @see #setItem(int, ItemStack)
	 */
	public boolean setItem(int slot, ItemStack item, GUIAction action) {
		if (inventory == null) return false;
		Preconditions.checkNotNull(item, "The item cannot be null");

		this.inventory.setItem(slot, item);
		if (action != null)
			this.actions.put(slot, action);
		
		return true;
	}

	/**
	 * Set / Replace an item in the GUI with no action
	 * 
	 * @param slot the slot to place the item
	 * @param item the item to add
	 * 
	 * @return true if successfully set. false otherwise
	 * @see #setItem(int, ItemStack, GUIAction)
	 */
	public boolean setItem(int slot, ItemStack item) {
		return this.setItem(slot, item, null);
	}
	
	/**
	 * Remove an action associated with the specified slot and optionally
	 * remove the item from the slot as well if one is present
	 * 
	 * @param slot the slot to remove the action from
	 * @param removeItem whether to remove the item from the slot or not
	 * 
	 * @see #removeActionForSlot(int)
	 */
	public void removeActionForSlot(int slot, boolean removeItem) {
		this.actions.remove(slot);
		if (removeItem) this.inventory.setItem(slot, null);
	}
	
	/**
	 * Remove an action associated with the specified slot.
	 * 
	 * @param slot the slot to remove the action from
	 * @see #removeActionForSlot(int, boolean)
	 */
	public void removeActionForSlot(int slot) {
		this.removeActionForSlot(slot, false);
	}

	/**
	 * Add a close hook that will be called before the inventory is closed
	 * 
	 * @param onClose the hook to add
	 */
	public void setOnClose(GUICloseHook onClose) {
		this.onClose = onClose;
	}
	
	/**
	 * Open the GUI for the specified player and invoke a consumable method
	 * before the inventory is actually opened
	 * 
	 * @param player the player to open the GUI for
	 * @param method the method to invoke before opening
	 */
	public void open(Player player, Consumer<Player> method) {
		if (inventory == null) return;
		Preconditions.checkNotNull(player, "Cannot open inventory for null player");
		
		if (method != null) 
			method.accept(player);
		
		PLAYER_GUIS.put(player.getUniqueId(), this);
		player.openInventory(inventory);
	}

	/**
	 * Open the GUI for the specified player
	 * 
	 * @param player the player to open the GUI for
	 */
	public void open(Player player) {
		this.open(player, null);
	}
	
	/**
	 * Clear all data from this GUI including its existing actions and its close hook
	 * 
	 * @param delete whether to delete the wrapped inventory or not. This should only
	 * be true if the {@link #clearAllLocalizedData()} method is being called onDisable
	 */
	public void clearData(boolean delete) {
		this.actions.clear();
		this.onClose = null;
		
		if (delete) {
			this.inventory = null;
		}
	}

	/**
	 * Initialise all basic static information regarding the GUI class such as
	 * listeners
	 * 
	 * @param plugin tour plugin instance
	 */
	public static void init(JavaPlugin plugin) {
		if (initialized) return;

		Bukkit.getPluginManager().registerEvents(new InventoryClickListener(), plugin);
		initialized = true;
	}
	
	/**
	 * Clear all data from existing GUIs
	 * 
	 * @see #clearData(boolean)
	 */
	public static void clearAllLocalizedData() {
		PLAYER_GUIS.forEach((uuid, gui) -> {
			Player player = Bukkit.getPlayer(uuid);
			if (player != null) player.closeInventory();
			
			gui.clearData(true);
		});
		
		PLAYER_GUIS.clear();
	}

	// Alternative interfaces / classes
	@FunctionalInterface
	public interface GUIAction {
		
		public boolean executeAction(Player player, GUI gui, ItemStack item, ItemStack holding, ClickType clickType);

	}

	@FunctionalInterface
	public interface GUICloseHook {

		public void executeHook(Player player, GUI gui);

	}

	private static class InventoryClickListener implements Listener {

		@EventHandler
		public void onClickGUI(InventoryClickEvent event) {
			Player player = (Player) event.getWhoClicked();
			GUI gui = PLAYER_GUIS.get(player.getUniqueId());

			if (gui == null) return;

			Inventory inventory = event.getClickedInventory();
			ItemStack item = event.getCurrentItem();
			int slot = event.getSlot();

			if ((inventory == player.getInventory() && (!event.getClick().toString().contains("SHIFT")))
					|| inventory == null || item == null) return;

			event.setCancelled(true);
			GUIAction action = gui.getActionForSlot(slot);
			
			if (action == null) return;
			
			if (action.executeAction(player, gui, item, event.getCursor(), event.getClick())) {
				player.closeInventory();
			}
		}

		@EventHandler
		public void onCloseGUI(InventoryCloseEvent event) {
			Player player = (Player) event.getPlayer();
			UUID uuid = player.getUniqueId();
			GUI gui = PLAYER_GUIS.get(uuid);

			if (gui == null) return;

			GUICloseHook onClose = gui.onClose;
			if (onClose != null) {
				onClose.executeHook(player, gui);
			}

			PLAYER_GUIS.remove(uuid);
		}
	}

}