package me.choco.crops.utils;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import com.google.common.collect.ClassToInstanceMap;
import com.google.common.collect.MutableClassToInstanceMap;

import me.choco.crops.api.CropModifier;
import me.choco.crops.api.ModifiedCrop;
import me.choco.crops.api.block.CustomBlock;
import me.choco.crops.api.breeding.CropBreedingRecipe;

import org.bukkit.Location;
import org.bukkit.block.Block;

/**
 * A manager to keep track of instances of modified crops. Used to
 * manage, clear, modify, retrieve and register crop data to the CropsReborn plugin
 */
public class CropManager {
	
	private final ClassToInstanceMap<CropModifier<?>> registeredModifiers = MutableClassToInstanceMap.create();
	private final Set<CropBreedingRecipe> recipes = new HashSet<>();
	
	private final Map<Location, ModifiedCrop> crops = new HashMap<>();
	private final Map<Location, CustomBlock> customBlocks = new HashMap<>();
	
	/**
	 * Register the crop to the registry map
	 * 
	 * @param crop the crop to register
	 */
	public void registerCrop(ModifiedCrop crop) {
		this.crops.put(crop.getCrop().getLocation(), crop);
	}
	
	/**
	 * Unregister the crop from the registry map
	 * 
	 * @param crop the crop to unregister
	 */
	public void unregisterCrop(ModifiedCrop crop) {
		this.crops.remove(crop.getCrop().getLocation());
	}
	
	/**
	 * Get an instance of the modified crop state
	 * 
	 * @param block the crop block
	 * @return an instance of the modified crop. null if not registered or not a crop
	 */
	public ModifiedCrop getCropData(Block block) {
		return getCropData(block.getLocation());
	}
	
	/**
	 * Get an instance of the modified crop state
	 * 
	 * @param location the crop location
	 * @return an instance of the modified crop. null if not registered or not a crop
	 */
	public ModifiedCrop getCropData(Location location) {
		return crops.get(location.getBlock().getLocation());
	}
	
	/**
	 * Get an instance of the modified crop state based on its UUID
	 * 
	 * @param uuid the UUID of the crop
	 * @return the modified crop
	 */
	public ModifiedCrop getCropData(UUID uuid) {
		return crops.values().stream()
			.filter(c -> c.getUUID().equals(uuid))
			.findFirst().orElse(null);
	}
	
	/**
	 * Check whether a crop has modifiers or not
	 * 
	 * @param block the crop block
	 * @return true if the crop is modified. false if the block is not a crop, nor registered
	 */
	public boolean isModifiedCrop(Block block) {
		return block != null && isModifiedCrop(block.getLocation());
	}
	
	/**
	 * Check whether a crop has modifiers or not
	 * 
	 * @param location the crop location
	 * @return true if the crop is modified. false if the location is not a crop, nor registered
	 */
	public boolean isModifiedCrop(Location location) {
		return location != null && crops.containsKey(location.getBlock().getLocation());
	}
	
	/**
	 * Get a set of all modified crops in the world
	 * 
	 * @return all modified crops
	 */
	public Collection<ModifiedCrop> getModifiedCrops() {
		return Collections.unmodifiableCollection(crops.values());
	}
	
	
	// Custom block methods
	
	/**
	 * Place and register a custom block in the world
	 * 
	 * @param block an instance of the block to place
	 * @return the instance of the block passed
	 */
	public CustomBlock placeCustomBlock(CustomBlock block) {
		if (isCustomBlock(block.getLocation()))
			throw new IllegalArgumentException("Cannot place custom block on existing custom block");
		
		block.placeInWorld();
		this.customBlocks.put(block.getLocation(), block);
		return block;
	}
	
	/**
	 * Destroy and unregister custom block at a given location
	 * 
	 * @param location the location of the custom block
	 * @return true if successfully broken
	 */
	public boolean destroyCustomBlock(Location location) {
		location = location.getBlock().getLocation();
		
		CustomBlock block = customBlocks.get(location);
		if (block == null) return false;
		
		block.destroy();
		this.customBlocks.remove(location);
		return true;
	}
	
	/**
	 * Check whether a location contains a custom block of a specific type or not
	 * 
	 * @param location the location to check
	 * @param type the type of block to check
	 * 
	 * @return true if a custom block of the given type is at the specified location
	 */
	public boolean isCustomBlock(Location location, Class<? extends CustomBlock> type) {
		CustomBlock block = customBlocks.get(location.getBlock().getLocation());
		return type != null ? type.isInstance(block) : block != null;
	}
	
	/**
	 * Check whether a location contains a custom block
	 * 
	 * @param location the location to check
	 * @return true if a custom block is at the specified location
	 */
	public boolean isCustomBlock(Location location) {
		return isCustomBlock(location, null);
	}
	
	/**
	 * Get an instance of a custom block at a specific location with a given type
	 * 
	 * @param location the location of the custom block
	 * @param type the type of the custom block
	 * 
	 * @return the custom block instance
	 * @throws IllegalStateException if the custom block is not of the specified type, or if no
	 * custom block is present
	 */
	public <T extends CustomBlock> T getCustomBlock(Location location, Class<T> type) {
		if (!isCustomBlock(location, type)) {
			throw new IllegalStateException("Custom block at location " + location.getBlockX() + ", " 
					+ location.getBlockY() + ", " + location.getBlockZ() + " is not of instance " + type.getSimpleName());
		}
		
		return type.cast(customBlocks.get(location.getBlock().getLocation()));
	}
	
	/**
	 * Get an instance of a custom block at a specific location
	 * 
	 * @param location the location of the block
	 * @return the custom block instance. null if none present
	 */
	public CustomBlock getCustomBlock(Location location) {
		return customBlocks.get(location.getBlock().getLocation());
	}
	
	/**
	 * Get all custom blocks of a specific type
	 * 
	 * @param type the type of block to check
	 * @return all registered custom blocks
	 */
	public <T extends CustomBlock> Set<T> getCustomBlocks(Class<T> type) {
		return customBlocks.values().stream()
				.filter(b -> type.isInstance(b))
				.map(type::cast)
				.collect(Collectors.toSet());
	}
	
	/**
	 * Get all custom blocks of varying types
	 * 
	 * @return all registered custom blocks
	 */
	public Collection<CustomBlock> getCustomBlocks() {
		return customBlocks.values();
	}
	
	
	// CropModifier registry methods
	
	/**
	 * Register a crop modifier to the modifier registry
	 * 
	 * @param modifier the modifier class
	 * @param instance an instance of the modifier (Provide an invalid value. i.e. -1 for numeric modifiers)
	 */
	public <T extends CropModifier<?>> void registerCropModifier(Class<T> modifier, T instance) {
		for (CropModifier<?> mod : this.registeredModifiers.values())
			if (mod.getName().equals(instance.getName())) throw new IllegalStateException("Crop modifiers names must be unique. (" + mod.getName() + ")");
		
		this.registeredModifiers.putInstance(modifier, instance);
	}
	
	/**
	 * Unregister a crop modifier from the modifier registry
	 * 
	 * @param modifier the modifier to unregister
	 */
	public void unregisterCropModifier(Class<? extends CropModifier<?>> modifier) {
		this.registeredModifiers.remove(modifier);
	}
	
	/**
	 * Check whether a modifier has been registered or not
	 * 
	 * @param modifier the modifier to check
	 * @return true if the modifier is registered
	 */
	public boolean isModifierRegistered(Class<? extends CropModifier<?>> modifier) {
		return registeredModifiers.containsKey(modifier);
	}
	
	/**
	 * Get an instance of a crop modifier. This instance should not be used
	 * as a modifier on any crops, as the values are invalid. These instances should
	 * be used to obtain informational data (i.e. calling methods that require an
	 * instance of the modifier, such as {@link CropModifier#getName()})
	 * 
	 * @param modifier the modifier to get an instance of
	 * @return an instance of the modifier, null if it has not been registered
	 */
	public <T extends CropModifier<?>> T getCropModifier(Class<T> modifier) {
		return modifier.cast(registeredModifiers.get(modifier));
	}
	
	/**
	 * Get an instance of a crop modifier based on its name. This instance should not 
	 * be used as a modifier on any crops, as the values are invalid. These instances
	 * should be used to obtain informational data (i.e. calling methods that require 
	 * an instance of the modifier, such as {@link CropModifier#getName()}
	 * 
	 * @param name the name of the modifier
	 * @return an instance of the modifier, null if none found
	 */
	public CropModifier<?> getCropModifier(String name) {
		for (CropModifier<?> modifier : this.registeredModifiers.values()) 
			if (modifier.getName().equals(name)) return modifier;
		return null;
	}
	
	/**
	 * Get a set of all registered crop modifiers
	 * 
	 * @return all registered modifiers
	 */
	public Set<Class<? extends CropModifier<?>>> getRegisteredCropModifiers() {
		return Collections.unmodifiableSet(registeredModifiers.keySet());
	}
	
	/**
	 * Add a crop breeding recipe, which will be maintained by crop supports
	 * 
	 * @param recipe the recipe to add
	 * @return true if the recipe was added
	 */
	public boolean addBreedingRecipe(CropBreedingRecipe recipe) {
		return recipes.add(recipe);
	}
	
	/**
	 * Remove a crop breeding recipe from the registry. Crop supports will no
	 * longer search for them
	 * 
	 * @param recipe the recipe to remove
	 * @return true if it was removed successfully
	 */
	public boolean removeBreedingRecipe(CropBreedingRecipe recipe) {
		return recipes.remove(recipe);
	}
	
	/**
	 * Get a set of all registered crop breeding recipes
	 * 
	 * @return all crop breeding recipes
	 */
	public Set<CropBreedingRecipe> getRegisteredRecipes() {
		return Collections.unmodifiableSet(recipes);
	}
	
	/**
	 * Clear all crop, modifier and recipe data
	 */
	public void clearCropData() {
		this.crops.clear();
		this.registeredModifiers.clear();
		this.recipes.clear();
	}
	
	/**
	 * Clear all registered custom block data of a specific block type
	 * 
	 * @param destroyBlocks whether to destroy the blocks or not
	 * @param type the type of block to clear
	 */
	@SuppressWarnings("unchecked")
	public void clearCustomBlockData(boolean destroyBlocks, Class<? extends CustomBlock> type) {
		Set<CustomBlock> customBlocks = (Set<CustomBlock>) this.getCustomBlocks(type);
		
		for (CustomBlock block : customBlocks) {
			if (destroyBlocks) block.destroy();
			this.customBlocks.remove(block.getLocation());
		}
	}
	
	/**
	 * Clear all registered custom block data and optional destroy them
	 * 
	 * @param destroyBlocks whether to destroy the blocks or not
	 */
	public void clearCustomBlockData(boolean destroyBlocks) {
		if (destroyBlocks) {
			this.customBlocks.values().forEach(CustomBlock::destroy);
		}
		
		this.customBlocks.clear();
	}
	
}