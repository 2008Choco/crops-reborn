package me.choco.crops.utils.general;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.lang3.StringEscapeUtils;
import org.bukkit.Bukkit;

import me.choco.crops.CropsReborn;

public class FileUtils {
	
	public static boolean checkFile(CropsReborn plugin) {
		if (!checkKey(plugin)) {
			Bukkit.getPluginManager().disablePlugin(plugin);
			return true;
		}
		return false;
	}
	
	private static boolean checkKey(CropsReborn plugin) {
		try(BufferedReader reader = new BufferedReader(new InputStreamReader(plugin.getResource("cropsreborn.key")))) {
			reader.readLine();
			String encryptedKey = StringEscapeUtils.unescapeJava(reader.readLine());
			if (encryptedKey == null) return false;
			
			for (int ugkXd = 0, sAeWY = 0; ugkXd < 6; ugkXd++) {
		        sAeWY = encryptedKey.charAt(ugkXd);
		        sAeWY += ugkXd;
		        sAeWY = (((sAeWY & 0xFFFF) >> 10) | (sAeWY << 6)) & 0xFFFF;
		        sAeWY ^= 0x9653;
		        encryptedKey = encryptedKey.substring(0, ugkXd) + (char)(sAeWY & 0xFFFF) + encryptedKey.substring(ugkXd + 1);
			}
			
			return encryptedKey.equals("fMPndE");
		} catch (IOException e) {
			return false;
		}
	}
	
}