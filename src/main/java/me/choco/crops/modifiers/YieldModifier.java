package me.choco.crops.modifiers;

import me.choco.crops.api.CropModifier;
import me.choco.crops.api.ModifiedCrop;
import me.choco.crops.api.breeding.CropBreedingRecipe;
import me.choco.crops.api.modtype.LimitedModifier;

public class YieldModifier extends CropModifier<Double> implements LimitedModifier<Double> {
	
	public YieldModifier(double value) {
		super(value);
	}
	
	@Override
	public float getResearchPercentage() {
		return 1.0f;
	}
	
	@Override
	public float getBreedingPercentage(CropBreedingRecipe recipe, ModifiedCrop... crops) {
		// TODO
		return 0.0f;
	}

	@Override
	public String getName() {
		return "Yield";
	}
	
	@Override
	public String getDescription() {
		return null;
	}
	
	@Override
	public Double getMaxValue() {
		return 10.0;
	}
	
	@Override
	public String serialize() {
		return "";
	}

	public static YieldModifier deserialize(String data) {
		return new YieldModifier(-1);
	}
}