package me.choco.crops.modifiers;

import me.choco.crops.api.CropModifier;
import me.choco.crops.api.ModifiedCrop;
import me.choco.crops.api.breeding.CropBreedingRecipe;
import me.choco.crops.api.modtype.LimitedModifier;

public class StrengthModifier extends CropModifier<Double> implements LimitedModifier<Double> {
	
	public StrengthModifier(double value) {
		super(value);
	}
	
	@Override
	public float getResearchPercentage() {
		return 1.0f;
	}
	
	@Override
	public float getBreedingPercentage(CropBreedingRecipe recipe, ModifiedCrop... crops) {
		// TODO
		return 0.0f;
	}
	
	@Override
	public Double getMaxValue() {
		return 10.0;
	}
	
	@Override
	public String getName() {
		return "Strength";
	}
	
	@Override
	public String getDescription() {
		return null;
	}
	
	@Override
	public String serialize() {
		return "";
	}

	public static StrengthModifier deserialize(String data) {
		return new StrengthModifier(-1);
	}
}