package me.choco.crops.modifiers;

import java.util.Random;

import me.choco.crops.api.CropModifier;
import me.choco.crops.api.ModifiedCrop;
import me.choco.crops.api.breeding.CropBreedingRecipe;
import me.choco.crops.api.modtype.LimitedModifier;
import me.choco.crops.api.modtype.RandomTickModifier;

public class SpeedModifier extends CropModifier<Double> implements LimitedModifier<Double>, RandomTickModifier {
	
	public SpeedModifier(double value) {
		super(value);
	}
	
	@Override
	public float getResearchPercentage() {
		return 1.0f;
	}
	
	@Override
	public float getBreedingPercentage(CropBreedingRecipe recipe, ModifiedCrop... crops) {
		// TODO
		return 0.0f;
	}
	
	@Override
	public Double getMaxValue() {
		return 10.0;
	}
	
	@Override
	public String getName() {
		return "Speed";
	}
	
	@Override
	public String getDescription() {
		return null;
	}
	
	@Override
	public void onRandomTick(ModifiedCrop crop, Random random) {
		
	}
	
	@Override
	public String serialize() {
		return "";
	}

	public static SpeedModifier deserialize(String data) {
		return new SpeedModifier(-1);
	}
}