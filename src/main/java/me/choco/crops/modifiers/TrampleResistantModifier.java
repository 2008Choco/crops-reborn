package me.choco.crops.modifiers;

import org.bukkit.ChatColor;

import me.choco.crops.api.CropModifier;
import me.choco.crops.api.ModifiedCrop;
import me.choco.crops.api.breeding.CropBreedingRecipe;

public class TrampleResistantModifier extends CropModifier<Boolean> {
	
	public TrampleResistantModifier(boolean value) {
		super(value);
	}
	
	@Override
	public float getResearchPercentage() {
		return 1.0f;
	}
	
	@Override
	public float getBreedingPercentage(CropBreedingRecipe recipe, ModifiedCrop... crops) {
		// TODO
		return 0.0f;
	}

	@Override
	public String getName() {
		return "Trample Resistant";
	}
	
	@Override
	public String getDescription() {
		return null;
	}
	
	@Override
	public ChatColor getAccentColour() {
		return value ? ChatColor.GREEN : ChatColor.RED;
	}
	
	@Override
	public String serialize() {
		return "";
	}
	
	public static TrampleResistantModifier deserialize(String data) {
		return new TrampleResistantModifier(false);
	}
}