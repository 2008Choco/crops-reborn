package me.choco.crops;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.lang3.EnumUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Server;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.java.JavaPlugin;

import me.choco.crops.api.CropModifier;
import me.choco.crops.api.CropType;
import me.choco.crops.api.ModifiedCrop;
import me.choco.crops.api.breeding.CropBreedingRecipe;
import me.choco.crops.blocks.CropSupports;
import me.choco.crops.blocks.SeedAnalyzer;
import me.choco.crops.blocks.task.CropSupportsTickTimer;
import me.choco.crops.blocks.task.SeedAnalyzerUpdateTask;
import me.choco.crops.events.SeedAnalyzerInteractionListener;
import me.choco.crops.events.SoilClickListener;
import me.choco.crops.modifiers.SpeedModifier;
import me.choco.crops.modifiers.StrengthModifier;
import me.choco.crops.modifiers.TrampleResistantModifier;
import me.choco.crops.modifiers.YieldModifier;
import me.choco.crops.utils.CropManager;
import me.choco.crops.utils.GUI;
import me.choco.crops.utils.general.FileUtils;
import me.choco.crops.utils.general.ItemBuilder;
import me.choco.nbt.NBTAPI;
import me.choco.nbt.nbt.NBTCompound;
import me.choco.nbt.nbt.NBTList;
import me.choco.nbt.types.NBTItem;
import me.choco.nbt.utils.ReflectionUtils;

public class CropsReborn extends JavaPlugin {
	
	/* TODO LIST:
	 *   - Create an agricultural book (Contains crop modifier information)
	 *   - Allow for a crop-breeding system similar to that of Agricraft
	 *   - Add a couple methods to the CropModifier class to determine the percent chance 
	 *     that the modifier can be discovered by research AND by crop breeding
	 *   - NBTTag parser for seed CropModifier data
	 *   	  "modifiers":[
	 *            {"name":"path.to.modifier.Class","value":"string_value"},
	 *            {"name":"path.to.modifier.Class2","value":"string_value"}
	 *        ]
	 */
	
	/* Per-chunk JSON Serialization Syntax 
	 * File name: "chunk.x.y.cr"
	 * {
	 *     "crops": {
	 *         // See ModifiedCrop serialization syntax
	 *     },
	 *     "analyzers": {
	 *         // See SeedAnalyzer serialization syntax
	 *     },
	 *     "supports": {
	 *         // See CropSupports serialization syntax
	 *     }
	 * }
	 */
	
	public static final Gson GSON = new GsonBuilder()
			.registerTypeAdapter(SeedAnalyzer.class, new SeedAnalyzer.TypeAdapter())
			.registerTypeAdapter(CropSupports.class, new CropSupports.TypeAdapter())
			.registerTypeAdapter(ModifiedCrop.class, new ModifiedCrop.TypeAdapter())
			.create();
	
	public static final ItemStack CROPNALYZER_ITEM = new ItemBuilder(Material.WATCH)
			.setName(ChatColor.YELLOW + "Cropnalyzer")
			.setLore(Arrays.asList(
					ChatColor.GREEN + "Analyzes crop data upon right click",
					ChatColor.GREEN + "Click a modified crop to view its data"))
			.addEnchantment(Enchantment.DURABILITY, 1)
			.addFlags(ItemFlag.HIDE_ENCHANTS)
			.build();
	
	private CropManager cropManager;
	
	private CropSupportsTickTimer randomTick;
	private SeedAnalyzerUpdateTask analyzeUpdate;
	
	@Override
	public void onEnable() {
		this.cropManager = new CropManager();
		if (FileUtils.checkFile(this)) return;
		GUI.init(this);
		
		// Initialize NBT-API as well
		String bukkitVersion = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
		ReflectionUtils.loadNMSClasses(bukkitVersion);
		
		// Register crop modifiers
		this.cropManager.registerCropModifier(SpeedModifier.class, new SpeedModifier(-1));
		this.cropManager.registerCropModifier(StrengthModifier.class, new StrengthModifier(-1));
		this.cropManager.registerCropModifier(YieldModifier.class, new YieldModifier(-1));
		this.cropManager.registerCropModifier(TrampleResistantModifier.class, new TrampleResistantModifier(true));
		
		// Register events
		Bukkit.getPluginManager().registerEvents(new SoilClickListener(this), this);
		Bukkit.getPluginManager().registerEvents(new SeedAnalyzerInteractionListener(this), this);
		
		// Prevent items from being used in crafting recipes (TODO: Move to its own class?)
		Bukkit.getPluginManager().registerEvents(new Listener() {
			@EventHandler
			public void onAttemptCraft(PrepareItemCraftEvent event) {
				ItemStack[] matrix = event.getInventory().getMatrix();
				for (ItemStack item : matrix) {
					if (CROPNALYZER_ITEM.isSimilar(item) || CropSupports.ITEM.isSimilar(item) || SeedAnalyzer.ITEM.isSimilar(item)) {
						event.getInventory().setResult(null);
					}
				}
			}
		}, this);
		
		// Add custom recipes
		this.registerCraftingRecipes();
		for (CropType type : CropType.values())
			this.cropManager.addBreedingRecipe(new CropBreedingRecipe(type).addCrop(type, 2));
		
		// TODO: Enable bStats (Metrics) & Update checker (SpiGet)
		// new Metrics(this);
		
		this.randomTick = CropSupportsTickTimer.startTimer(this);
		this.analyzeUpdate = SeedAnalyzerUpdateTask.startTimer(this);
	}
	
	@Override
	public void onDisable() {
		if (cropManager != null) {
			this.cropManager.clearCropData();
			this.cropManager.clearCustomBlockData(true);
		}
		if (randomTick != null) this.randomTick.cancel();
		if (analyzeUpdate != null) this.analyzeUpdate.cancel();
		
		GUI.clearAllLocalizedData();
	}
	
	/**
	 * Get the main instance of the crop manager
	 * 
	 * @return the crop manager
	 */
	public CropManager getCropManager() {
		return cropManager;
	}
	
	/**
	 * Send a message to a player with the CropsReborn prefix
	 * 
	 * @param sender the player to send the message to
	 * @param message the message to send
	 */
	public void sendMessage(CommandSender sender, String message) {
		sender.sendMessage(ChatColor.GRAY + message);
	}

	/**
	 * Whether a crop item (seed) has been researched or not
	 * 
	 * @param item the item to check
	 * @return true if it has been researched, false otherwise
	 */
	public static boolean isResearchedCropItem(ItemStack item) {
		if (item == null || !CropType.isCropSeed(item.getType())) return false;
		return NBTAPI.getNBTItem(item).hasKey("modifiers");
	}
	
	/**
	 * Research an ItemStack (presumably a crop item) to be modified with
	 * custom NBT data and {@link CropModifier}s. The resulting crop will
	 * be capable of being placed in the world
	 * 
	 * @param item the crop to modify
	 * @return the modified crop item
	 */
	public static ItemStack researchCropItem(ItemStack item) {
		if (isResearchedCropItem(item)) return item;
		
		List<String> modifierLore = new ArrayList<>();
		NBTList modifierRoot = new NBTList();
		ThreadLocalRandom random = ThreadLocalRandom.current();
		
		CropsReborn plugin = JavaPlugin.getPlugin(CropsReborn.class);
		plugin.cropManager.getRegisteredCropModifiers().stream()
				.map(m -> plugin.cropManager.getCropModifier(m))
				.filter(CropModifier::isResearchable)
				.filter(m -> random.nextFloat() <= m.getResearchPercentage())
				.forEach(m -> {
					NBTCompound modifierCompound = new NBTCompound();
					modifierCompound.setString("name", m.getClass().getName());
					modifierCompound.setString("value", String.valueOf(m.getModifierValue()));
					
					modifierRoot.addNBTValue(modifierCompound);
					modifierLore.add(m.getBaseColour() + m.getName() + ": " + m.getAccentColour() + m.getModifierValue());
				});
		
		NBTItem researchedItem = new NBTItem(new ItemBuilder(item).setLore(modifierLore).build());
		researchedItem.setNBTValue("modifiers", modifierRoot);
		return researchedItem.getModifiedItemStack();
	}
	
	@SuppressWarnings("deprecation")
	private void registerCraftingRecipes() {
		Server server = Bukkit.getServer();
		
		// Crop supports
		ItemStack cropSupportsResult = CropSupports.ITEM.clone();
		cropSupportsResult.setAmount(4);
		
		server.addRecipe(new ShapedRecipe(new NamespacedKey(this, "cropsupports1"), cropSupportsResult).shape("SS ", "SS ", "   ").setIngredient('S', Material.STICK));
		server.addRecipe(new ShapedRecipe(new NamespacedKey(this, "cropsupports2"), cropSupportsResult).shape("   ", "SS ", "SS ").setIngredient('S', Material.STICK));
		server.addRecipe(new ShapedRecipe(new NamespacedKey(this, "cropsupports3"), cropSupportsResult).shape(" SS", " SS", "   ").setIngredient('S', Material.STICK));
		server.addRecipe(new ShapedRecipe(new NamespacedKey(this, "cropsupports4"), cropSupportsResult).shape("   ", " SS", " SS").setIngredient('S', Material.STICK));
		
		// Cropnalyzer
		server.addRecipe(new ShapedRecipe(CROPNALYZER_ITEM)
				.shape("WTB", "GIG", "CIP").setIngredient('W', Material.SEEDS)
				.setIngredient('T', Material.REDSTONE_TORCH_ON).setIngredient('B', Material.BEETROOT_SEEDS)
				.setIngredient('G', Material.GLASS).setIngredient('I', Material.IRON_INGOT)
				.setIngredient('C', Material.CARROT_ITEM).setIngredient('P', Material.POTATO_ITEM));
		
		// Seed Analyzer
		server.addRecipe(new ShapedRecipe(SeedAnalyzer.ITEM)
				.shape("SPS", " QS", "WHW").setIngredient('S', Material.STICK)
				.setIngredient('P', Material.THIN_GLASS).setIngredient('Q', Material.STEP)
				.setIngredient('W', Material.QUARTZ_BLOCK).setIngredient('H', new MaterialData(Material.STEP, (byte) 7)));
	}
	
	
	// TODO: TESTING COMMANDS
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) return true;
		
		Player player = (Player) sender;
		
		if (cmd.getName().equalsIgnoreCase("supports")) {
			if (args.length == 0) {
				player.sendMessage(ChatColor.RED + "/supports <create|delete>");
				return true;
			}
			
			if (args[0].equalsIgnoreCase("create")) {
				this.cropManager.placeCustomBlock(new CropSupports(player.getLocation().add(0, 0.2, 0)));
				
				player.sendMessage(ChatColor.GREEN + "Crop supports placed in world");
			}
			
			else if (args[0].equalsIgnoreCase("delete")) {
				this.cropManager.clearCustomBlockData(true, CropSupports.class);
				
				player.sendMessage(ChatColor.GREEN + "Deleted all supports placed in the world");
			}
		}
		
		else if (cmd.getName().equalsIgnoreCase("analyzer")) {
			if (args.length == 0) {
				player.sendMessage(ChatColor.RED + "/analyzer <create|delete> [direction]");
				return true;
			}
			
			if (args[0].equalsIgnoreCase("create")) {
				BlockFace direction = BlockFace.NORTH;
				if (args.length >= 2) {
					if (!EnumUtils.isValidEnum(BlockFace.class, args[1].toUpperCase())) {
						player.sendMessage(ChatColor.RED + "Invalid direction. Expected: [NORTH, EAST, SOUTH, WEST]");
						return true;
					}
					
					direction = BlockFace.valueOf(args[1].toUpperCase());
					if (direction != BlockFace.NORTH && direction != BlockFace.EAST
							&& direction != BlockFace.SOUTH && direction != BlockFace.WEST) {
						player.sendMessage(ChatColor.RED + "Invalid direction, \"" + args[1] + "\". Expected: [NORTH, EAST, SOUTH, WEST]");
						return true;
					}
				}
				
				this.cropManager.placeCustomBlock(new SeedAnalyzer(player.getLocation(), direction));
				player.sendMessage(ChatColor.GREEN + "Seed Analyzer placed in world");
			}
			
			else if (args[0].equalsIgnoreCase("delete")) {
				this.cropManager.clearCustomBlockData(true, SeedAnalyzer.class);
				
				player.sendMessage(ChatColor.GREEN + "Deleted all analyzers placed in the world");
			}
		}
		
		else if (cmd.getName().equalsIgnoreCase("seeds")) {
			List<String> lore = this.cropManager.getRegisteredCropModifiers().stream().map(m -> {
				CropModifier<?> modifier = this.cropManager.getCropModifier(m);
				return modifier.getBaseColour() + modifier.getName() + ": " + modifier.getAccentColour() + modifier.getModifierValue();
			}).collect(Collectors.toList());
			
			ItemStack seed = new ItemBuilder(Material.SEEDS).setLore(lore).build();
			seed = NBTAPI.getNBTItem(seed).setString("modifiers", "temp").getModifiedItemStack(); // TEMP for testing. These should be capable of being placed
			
			player.getInventory().addItem(seed);
		}
		
		return true;
	}
}