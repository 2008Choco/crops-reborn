package me.choco.crops.events;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import me.choco.crops.CropsReborn;
import me.choco.crops.blocks.SeedAnalyzer;
import me.choco.crops.utils.CropManager;

public class SeedAnalyzerInteractionListener implements Listener {
	
	private final CropManager cropManager;
	
	public SeedAnalyzerInteractionListener(CropsReborn plugin) {
		this.cropManager = plugin.getCropManager();
	}
	
	@EventHandler
	public void onPlaceSeedAnalyzer(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		ItemStack item = event.getItemInHand();
		
		if (!SeedAnalyzer.ITEM.isSimilar(item)) return;
		
		this.cropManager.placeCustomBlock(new SeedAnalyzer(event.getBlock().getLocation(), this.getOpposingDirection(player)));
	}
	
	@EventHandler
	public void onDestroySeedAnalyzer(BlockBreakEvent event) {
		Location location = event.getBlock().getLocation();
		
		if (!cropManager.isCustomBlock(location, SeedAnalyzer.class)) return;
		
		this.cropManager.destroyCustomBlock(location);
		if (event.getPlayer().getGameMode() != GameMode.CREATIVE)
			location.getWorld().dropItemNaturally(location.add(0.5, 0, 0.5), SeedAnalyzer.ITEM);
	}
	
	@EventHandler
	public void onInteractWithSeedAnalyzer(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		
		Block block = event.getClickedBlock();
		if (block == null) return;
		Location location = event.getClickedBlock().getLocation();
		
		if (!cropManager.isCustomBlock(location, SeedAnalyzer.class)) return;
		
		SeedAnalyzer analyzer = this.cropManager.getCustomBlock(location, SeedAnalyzer.class);
		analyzer.onClick(player, event);
	}
	
	private BlockFace getOpposingDirection(Player player) {
		float yaw = player.getLocation().getYaw();
		
		if (yaw > 45 && yaw <= 135) return BlockFace.EAST;
		else if (yaw > 135 && yaw <= 225) return BlockFace.SOUTH;
		else if (yaw > 225 && yaw <= 315) return BlockFace.WEST;
		else return BlockFace.NORTH;
	}
	
}