package me.choco.crops.events;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import me.choco.crops.CropsReborn;
import me.choco.crops.api.CropType;
import me.choco.crops.blocks.CropSupports;
import me.choco.crops.utils.CropManager;
import me.choco.nbt.NBTAPI;
import me.choco.nbt.types.NBTItem;

public class SoilClickListener implements Listener {
	
	private final CropsReborn plugin;
	private final CropManager cropManager;
	
	public SoilClickListener(CropsReborn plugin) {
		this.plugin = plugin;
		this.cropManager = plugin.getCropManager();
	}
	
	@EventHandler
	public void onAttemptPlaceCropSupports(PlayerInteractEvent event) {
		if (event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
		
		Block block = event.getClickedBlock();
		Player player = event.getPlayer();
		Material type = block.getType();
		ItemStack item = event.getItem();
		
		if (type != Material.SOIL && type != Material.SOUL_SAND) return;
		if (event.getBlockFace() != BlockFace.UP) return;
		
		Location supportLocation = block.getRelative(BlockFace.UP).getLocation();
		
		// Place supports
		if (!player.isSneaking()) {
			if (!CropSupports.ITEM.isSimilar(item)) return;
			
			if (CropType.isCrop(supportLocation.getBlock().getType())){
				plugin.sendMessage(player, "There is already a crop planted here!");
				return;
			}
			else if (this.cropManager.isCustomBlock(supportLocation, CropSupports.class)){
				plugin.sendMessage(player, "There are already crop supports here!");
				return;
			}
			
			this.cropManager.placeCustomBlock(new CropSupports(supportLocation));
			player.playSound(supportLocation, Sound.ITEM_SHOVEL_FLATTEN, 1, 1);
			player.playSound(supportLocation, Sound.BLOCK_GRASS_BREAK, 1, 1);
			
			if (player.getGameMode() != GameMode.CREATIVE)
				this.decrementItemCount(item);
		}
		
		// Destroy supports
		else {
			if (this.cropManager.destroyCustomBlock(supportLocation)) {
				if (player.getGameMode() != GameMode.CREATIVE)
					supportLocation.getWorld().dropItemNaturally(supportLocation.add(0.5, 0.5, 0.5), CropSupports.ITEM);
				player.playSound(supportLocation, Sound.ITEM_HOE_TILL, 1, 0);
			}
		}
	}
	
	@EventHandler
	public void onCropInspect(PlayerInteractEvent event) {
		Action action = event.getAction();
		if ((action != Action.RIGHT_CLICK_BLOCK && action != Action.LEFT_CLICK_BLOCK)) return;
		
		Block block = event.getClickedBlock();
		Player player = event.getPlayer();
		
		if (!CropsReborn.CROPNALYZER_ITEM.isSimilar(event.getItem())) return;
		
		Material type = block.getType();
		if (!CropType.isCrop(type)) return;
		
		if (this.cropManager.isModifiedCrop(block)) {
			// TODO: Analyse crops
			player.sendMessage(ChatColor.YELLOW + "Crop information for crop " + ChatColor.GOLD + CropType.getCropType(type).getName());
			player.sendMessage(ChatColor.GREEN + ChatColor.BOLD.toString() + "  TODO: " + ChatColor.DARK_RED + "(Information to be added)");
		}
		else {
			player.sendMessage(ChatColor.RED + ChatColor.BOLD.toString() + "  ERROR: " + ChatColor.DARK_RED + "Invalid crop definition");
		}
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onPlaceModifiedCrop(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		ItemStack item = event.getItemInHand();
		Block blockAgainst = event.getBlockAgainst();
		
		if (item == null || !CropType.isCropSeed(item.getType()) || blockAgainst.getType() != Material.SOIL) return;
		
		// Placing researched seed
		NBTItem nbtItem = NBTAPI.getNBTItem(item);
		if (nbtItem.hasKey("modifiers")) {
			// TODO: Get the modifiers from NBT data, create a ModifiedCrop object and register it
		}
		
		// Placing non-researched seeds
		else {
			plugin.sendMessage(player, ChatColor.RED + "You cannot place crop seeds before researching them");
			event.setCancelled(true);
		}
	}
	
	private void decrementItemCount(ItemStack item) {
		int amount = item.getAmount();
		item.setAmount(amount > 1 ? amount - 1 : 0);
	}
}